
let params = new URLSearchParams(window.location.search);


let productId = params.get('productId');

let productName = document.querySelector('#productName');
let productDesc = document.querySelector('#productDesc');
let productPrice = document.querySelector('#productPrice');
let productContainer = document.querySelector('#productContainer');
let imageContainer = document.getElementById('imageContainer');
let token = localStorage.getItem('token');

fetch(`http://localhost:4000/api/products/getSingleProduct/${productId}`, 
		{
			method: "GET",
			headers: {
				"Authorization":`Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then(result => {

	productName.innerHTML = result.name

	productDesc.innerHTML = result.description
	productPrice.innerHTML = result.price
	if(token){
		productContainer.innerHTML = `
			<button class="btn btn-block" id="orderButton">Add to Cart</button>
		`
	} else {
		productContainer.innerHTML = `<a class="btn btn-block" href="./login.html">Login to Add</a>`
	}
		imageContainer.innerHTML = `<img src=${result.image} class="img-fluid" />`

	let orderButton = document.querySelector('#orderButton');
	orderButton.addEventListener("click", () => {
	let orderQuantity = document.getElementById("quantity").value ? parseInt(document.getElementById("quantity").value) : 1;
		console.log(orderQuantity)
		let productOrder = {
			productId: productId,
			productImage: result.image,
			productName: result.name,
			quantity: Number(orderQuantity),
			productPrice: Number(result.price)
		}

		Swal.fire(
		  'Thank you!',
		  `Added ${productOrder.quantity} ${result.name} to your cart!`,
		  'success'
		)
		let cartArr = JSON.parse(localStorage.getItem("cart"));
		if(cartArr.length === 0){
			cartArr.push(productOrder);
			localStorage.setItem("cart",JSON.stringify(cartArr));

		} else {
			let foundIndex = cartArr.findIndex(product => product.productId === productOrder.productId);
			if(foundIndex >= 0){
				cartArr[foundIndex].quantity+=productOrder.quantity;
				localStorage.setItem("cart",JSON.stringify(cartArr));
				console.log(localStorage.getItem("cart"));
			} else {
				cartArr.push(productOrder);
				localStorage.setItem("cart",JSON.stringify(cartArr));
				console.log(localStorage.getItem("cart"));
			}
		}

	})



});