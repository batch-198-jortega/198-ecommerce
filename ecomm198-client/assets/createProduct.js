let createProduct = document.querySelector('#createProduct')

createProduct.addEventListener("submit", (e) => {

	//to access the files uploaded from a selected file upload input element, access the files property of the element.
	//It is an array that contains the files uploaded.
	e.preventDefault()
	const selectedFile = document.getElementById('input').files[0];
	console.log(selectedFile);

	//formData() is used here to properly pass data into the cloudinary api to save our file.
	//It is not advisable to save pictures in mongodb documents as they have limited sizes.
	//It is also not advisable to save pictures in heroku as heroku resets the api/server every end of the day.
	//Setting up your cloudinary account:
	//https://css-tricks.com/image-upload-manipulation-react/
	//You can follow the cloudinary file upload tutorial using fetch here:
	//https://blog.johnnyreilly.com/2018/03/25/uploading-images-to-cloudinary-with-fetch
	const cloudinary = new FormData();
	cloudinary.append("file", selectedFile);
	cloudinary.append("upload_preset", "sample")
	cloudinary.append("cloud_name","deghtqf0b")


	fetch("https://api.cloudinary.com/v1_1/deghtqf0b/image/upload",{
		method:"post",
		body: cloudinary
		})
		.then(res => res.json())
		.then(data => {

			//cloudinary returns the url of the image from its servers
			//console.log(data.url);
			let productName = document.querySelector('#productName').value
			let productPrice = document.querySelector('#productPrice').value
			let productDesc = document.querySelector('#productDesc').value
			if(productName !== "" && productPrice !== "" && productDesc !== ""){
				
				let token = localStorage.getItem("token"); 

				fetch("http://localhost:4000/api/products/", 
					{
						method: "POST",
						headers: {
							"Content-Type" : "application/json",
							"Authorization" : `Bearer ${token}` 
						},
						body: JSON.stringify({
							image: data.url,
							name: productName,
							price: productPrice,
							description: productDesc
						})

					}
				)
				.then(result => result.json())
				.then(result => {
					if(result){
						Swal.fire(
						  'Item Added Successfully!',
						  `Item ${productName} has been added. `,
						  'success'
						).then(()=>window.location.assign('./products.html'))
					} else {
						Swal.fire(
						  'Something Went Wrong!',
						  `Item ${productName} not added. `,
						  'error'
						).then(()=>window.location.assign('./products.html'))
					}
				})
			}

		})
})