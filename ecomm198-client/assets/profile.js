//.getItem() allows us to get an item from the localStorage using its key.
//syntax: localStorage.getItem("key")
//Note: since items are stored as JSON in the localStorage, for other data types as number,boolean,arrays and objects, we have to parse them back as JS objects using JSON.parse.
let token = localStorage.getItem('token')

let name = document.querySelector('#name');
let email = document.querySelector('#email');
let mNum = document.querySelector('#mobileNum');
let productContainer = document.querySelector('#productContainer')
let totalDueHTML = document.querySelector('#totalDue')
let totalDue = 0
fetch('http://localhost:4000/api/users/getUserDetails',
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then(result => {
		name.innerHTML = `${result.firstName} ${result.lastName}`;
		email.innerHTML = result.email;
		mNum.innerHTML = result.mobileNo;

		if (result.isAdmin != true) {
			
			fetch('http://localhost:4000/api/orders/getUserOrders',{

				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}


			})
			.then(res => res.json())
			.then(data => {
				
				let count = 0;
				data.forEach(order => {
						count++;
						let date = new Date(order.purchasedOn).toLocaleString();
						productContainer.innerHTML +=
							`
								<div class="card my-5 py-5">
									<p class="card-title">Date Purchased: ${date}</p>
									<p class="card-title">Order ID: ${order._id}</p>				
									<p class="card-title">Total: &#8369; ${order.totalAmount}</p>
								</div>
							`


				})

			})

			} else {
				productContainer.innerHTML += `
				<h5 class="mt-3">No Orders Available.</h5>
				`

			}
})
