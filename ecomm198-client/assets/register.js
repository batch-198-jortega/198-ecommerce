let registerForm = document.querySelector('#registerUser')
registerForm.addEventListener('submit', (e) => {
	e.preventDefault();
	//In JS, the attached HTML is considered as an object, thus the DOM, or Document Object Model.
	//We can access the whole document and its elements through the built-in document object.
	//The document object has a method called querySelector() which allow us to select an html through its id, class using css notation.
	//We can then have access to the element, here in JS.
	//.value is the current value of an input element selected
	let firstName = document.querySelector('#firstName').value;
	let lastName = document.querySelector('#lastName').value;
	let email = document.querySelector('#email').value;
	let mobile = document.querySelector('#mobile').value;
	let password = document.querySelector('#password').value;
	let confirmPassword = document.querySelector('#confirmPassword').value;

	if(password === confirmPassword && password !== "" && confirmPassword !== ""){

		//fetch() is a JS method which allow us to pass/create a request to an api.
		//syntax: fetch("<requestURL>",{options})
		//method: the request's HTTP method.
		//headers: additional information about our request.
		//"Content-Type": "application/json" denotes that the request body contains JSON.
		//we JSON.stringify the body of our request to be able to send a JSON format string to our API.
		fetch("http://localhost:4000/api/users/checkEmailExists",
			{
				method: "POST",
				headers: {
					"Content-Type" : "application/json"
				},
				body: JSON.stringify({
					email : email
				})
			}
		)
		.then(res => result.json())
		.then(data => {
			//data is the response of the api/server after it's been process as a JS object through the previous res.json() method.
			//data contain the response from our api.
			//console.log(data)
			if(data === false){

				fetch("http://localhost:4000/api/users/",
					{
						method: "POST",
						headers: {
							"Content-Type" : "application/json"
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobile: mobile,
							password: password
						})
					}
				)
				.then(res => res.json())
				.then(data => {
					//Swal is a package imported from sweetalert2. It is used as an alternative to alert windows.
					//.fire() takes 3 arguments, the title, the message and the icon.
					//window.location.assign() allows us to redirect the user to another page.
					if(data === true){
						Swal.fire(
						  'Welcome to the Zuitt Shop',
						  `Thank you for registering!`,
						  'success'
						).then(()=>window.location.assign('./login.html'))
					
					} else if(data.message) {
						Swal.fire(
						  'Something Went Wrong!',
						  `Registration Failed. `,
						  'error'
						).then(()=>window.location.assign('./products.html'))
					}
				})
			}  else {
				Swal.fire(
				  'Something Went Wrong!',
				  `The email you're trying to register already exists.`,
				  'error'
				).then(()=>window.location.assign('./products.html'))
			}
		})
	}
})