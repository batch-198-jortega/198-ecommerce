let loginUser = document.querySelector('#loginUser');
//.addEventLister() is attached to a selected element to allow us to listen/anticipate interactions from a user.
//.addEventListener("event",function)
//event represents the action the user did. In our case, when the form was submitted through the button, we ran the following function.
loginUser.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector('#email').value
	let password = document.querySelector('#password').value

	if(email === "" || password === ""){
		alert('Your email or password is missing.')
	} else {
		fetch("http://localhost:4000/api/users/login", 
			{
				method: "POST",
				headers: {
					"Content-Type" : "application/json"
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			}
		)
		.then(res => res.json())
		.then(data => {
			let token = data.accessToken
			if(token){
				localStorage.setItem("token", data.accessToken);
				fetch("http://localhost:4000/api/users/getUserDetails", 
					{
						method: "GET",
						headers: {
							"Authorization" : `Bearer ${token}`
						}
					}
				)
				.then(res => res.json())
				.then(data => {
					//localStorage is a JSON storage in your browser.
					//It allows us to store data within your browser as JSON.
					//.setItem() allows us to set data with key in the localStorage
					//syntax: localStorage.setItem("key",data)
					//An empty array is set as the user's cart but must first be stringified as JSON or else, JS will forcibly turn it into a string before adding it in the localStorage.
					localStorage.setItem("id", data._id);
					localStorage.setItem("isAdmin", data.isAdmin);
					localStorage.setItem("cart",JSON.stringify([]));	
					Swal.fire(
					  'Welcome to the Zuitt Shop',
					  `Thank you for logging in!`,
					  'success'
					).then(()=>window.location.assign('./products.html'))
				})
			} else {
				Swal.fire(
				  'Login Failed',
				  `Wrong Credentials!`,
				  'error'
				).then(()=>window.location.assign('./login.html'))
			}
		})
	}
})