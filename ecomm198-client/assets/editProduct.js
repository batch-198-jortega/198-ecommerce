let editProduct = document.querySelector('#editProduct');
let params = new URLSearchParams(window.location.search)
let productId = params.get('productId')

let token = localStorage.getItem('token')

let productImage = document.querySelector('#oldImageContainer');
let productName = document.querySelector('#productName');
let productPrice = document.querySelector('#productPrice');
let productDesc = document.querySelector('#productDesc');

fetch(`http://localhost:4000/api/products/getSingleProduct/${productId}`,
	{
		method: "GET",
		headers: {
			"Authorization" : `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then(result => {
	// console.log(result)
	productName.value = result.name
	productPrice.value = result.price
	productDesc.value = result.description
	productImage.innerHTML += `<img src="${result.image}" class="img-fluid">`
})

editProduct.addEventListener("submit", (e) => {

	e.preventDefault()
	const selectedFile = document.getElementById('input').files[0];
	console.log(selectedFile);
	const cloudinary = new FormData();
	cloudinary.append("file", selectedFile);
	cloudinary.append("upload_preset", "sample")
	cloudinary.append("cloud_name","deghtqf0b")

	productName = productName.value
	productDesc = productDesc.value
	productPrice = productPrice.value
	fetch("https://api.cloudinary.com/v1_1/deghtqf0b/image/upload",{
		method:"post",
		body: cloudinary
		})
		.then(res => res.json())
		.then(data => {
			fetch(`http://localhost:4000/api/products/${productId}`, 
				{
					method: "PUT",
					headers: {
						"Content-Type" : "application/json",
						"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					image: data.url,
					name: productName,
					description: productDesc,
					price: productPrice
				})
			}
			)
			.then(result => result.json())
			.then(result => {
				if(result !== "undefined"){
					Swal.fire(
					  'Item Updated Successfully!',
					  `Item ${productName} has been updated. `,
					  'success'
					).then(()=>window.location.assign('./products.html'))
				} else {
					Swal.fire(
					  'Something Went Wrong!',
					  `Item cannot be udpated.`,
					  'error'
					).then(()=>window.location.assign('./products.html'))
				}
			})
		})

})