let token = localStorage.getItem("token");
let admin = localStorage.getItem("isAdmin") === "true";
let adminButton = document.querySelector('#adminButton')
let cardFooter;
let footer = document.getElementById("footer");

//.getElementById is another way to select an html element using its id.
//.innerHTML contains the children/content of a selected html element as a string.
//By assigning the innerHTML of an element we can add children to an element.
if(!admin){
	adminButton.innerHTML = null
} else {
	adminButton.innerHTML =
	`
		<div class="col-12" id="cpButton">
			<a href="./createProduct.html" class="btn">Create Product
			</a>
		</div>
	`
}
fetch("http://localhost:4000/api/products/", 
	{
		method: "GET",
		headers: {
			"Authorization" : `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then(result => {
	if(result.length < 1){
		productData = `No Products Available`
		//.classList contains the classes of a selected HTML element.
		//toggle() allows us to add/remove a class from the element.
		footer.classList.toggle("fixed-bottom");
	} else {
		productData = result.map((product) => {
			

			if(admin === false || !admin){
				cardFooter = 
			`
				<a href="./viewProduct.html?productId=${product._id}" class="btn btn-block selectButton" >View Product</a>
			`
			} else {

				if(product.isActive === true){
					//onclick attribute allows us to add an onclick event on an element. It works similarly to addEventListener.
					//onclick="function()"
					cardFooter =
					`
						<a href="./editProduct.html?productId=${product._id}" class="btnEdit btn-block text-center editButton">Edit Product</a>
						
						<a href="#" class="btnDelete btn-block text-center deleteButton" onclick="archive('${product._id}')">Archive Product</a>
					`

				} else {
					cardFooter =
					`
						<a href="./editProduct.html?productId=${product._id}" class="btnEdit btn-block text-center editButton">Edit Product</a>
						
						<a href="#" class="btnDelete btn-block text-center deleteButton" onclick="activate('${product._id}')">Activate Product </a>
					`

				}
			}

			// map images
			return(
				`
				<div class="card-container col-12 col-md-4 col-lg-3 my-5">
					<div class="card productCard">
						<div class="card-body">
							<img src="${product.image}">
							<h5 class="card-title mt-3 text-left">
								${product.name}
							</h5>
							<p class="card-text text-left">
								₱ ${product.price}
							</p>
						</div>
						<div class="card-footer">
								${cardFooter}

						</div>
					</div>
				</div>

				`

				)
			
		}).join('')
		footer.classList.toggle("fixed-bottom");
	}

	let container = document.querySelector('#productContainer')
	container.innerHTML = productData
})

function archive(productId){
	fetch(`http://localhost:4000/api/products/archive/${productId}`, 
		{
			method: "DELETE",
			headers: {
				"Authorization" : `Bearer ${token}`
			}
		}
	)
	.then(result => result)
	.then(result => {
		if(result){
			Swal.fire(
			  'Item Archived Successfully!',
			  `Item has been archived. `,
			  'success'
			).then(()=>window.location.assign('./products.html'))
		} else {
			Swal.fire(
			  'Something Went Wrong!',
			  `Item not archived. `,
			  'error'
			).then(()=>window.location.assign('./products.html'))
		}
	})
}

function activate(productId){
	fetch(`http://localhost:4000/api/products/activate/${productId}`, 
		{
			method: "PUT",
			headers: {
				"Authorization" : `Bearer ${token}`
			}
		}
	)
	.then(result => result)
	.then(result => {
		if(result){
			Swal.fire(
			  'Item Activated Successfully!',
			  `Item has been activated. `,
			  'success'
			).then(()=>window.location.assign('./products.html'))
		} else {
			Swal.fire(
			  'Something Went Wrong!',
			  `Item not activated. `,
			  'error'
			).then(()=>window.location.assign('./products.html'))
		}
	})
}