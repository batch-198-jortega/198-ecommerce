let cartBody = document.getElementById("cartBody");
let totalCart = document.getElementById("totalCart");
let deleteCart = document.getElementById("deleteCart");
let checkoutBtn = document.getElementById("checkoutBtn");

let totalCart2 = document.getElementById("totalCart2");
let deleteCart2 = document.getElementById("deleteCart2");
let checkoutBtn2 = document.getElementById("checkoutBtn2");

let cartContainer = document.getElementById("cart");
let checkoutMobile = document.getElementById("checkoutMobile");
let cart = JSON.parse(localStorage.getItem("cart"));
//Note: since items are stored as JSON in the localStorage, for other data types as number,boolean,arrays and objects, we have to parse them back as JS objects using JSON.parse.
let token = localStorage.getItem("token");
let footer = document.getElementById("ftr");

function deleteItem(id){
	let index = cart.findIndex(productOrder => productOrder.productId === id);
	cart.splice(index,1);
	localStorage.setItem("cart",JSON.stringify(cart));
	location.reload();
}
if(token && cart.length > 0){
	footer.classList.toggle("fixed-bottom");
	let total = cart.reduce((x,item)=>x+=(item.productPrice*item.quantity),0);
	totalCart.innerHTML += total
	totalCart2.innerHTML += total
	cart.forEach(productOrder => {

		let subtotal = productOrder.productPrice * productOrder.quantity;

		cartBody.innerHTML += `

						<div class="card container-fluid px-4 mb-4">
							<div class="row pt-1">
								<div class="col-4 p-3 p-md-1 mx-0 text-center">
									<img class="img-fluid" src="${productOrder.productImage}"/>
								</div>
								<div class="col-8 mx-0">
									<div class="card-body px-1 py-2">
										<h5 class="card-title text-left">
											${productOrder.productName}
										</h5>
										<p class="card-text text-left">
											₱ ${productOrder.productPrice}
										</p>
										<p class="card-text text-left">
											 Quantity: ${productOrder.quantity}
										</p>
										<p class="card-text text-left">
											 Subtotal: ₱ ${productOrder.quantity * productOrder.productPrice}
										</p>
									</div>
								</div>
							</div>
							<div class="card-footer px-1 pt-2">
								<button class="btn btn-block btn-danger" id="itemDel" onclick="deleteItem('${productOrder.productId}')">Delete Item</button>
							</div>
						</div>

		`
	})
} else {
	footer.classList.toggle("fixed-bottom");
	cartContainer.innerHTML = `

		<h1 class="text-center mt-5 mb-4">No items in your cart</h1>
		<div class="row text-center mb-md-4 pb-4">
			<div class="col-md-10 mx-auto">
				<img src="../picref/empty-cart.gif" class="img-fluid"/>
			</div>
		</div>

	`
	checkoutMobile.innerHTML = ``

}


deleteCart.addEventListener('click',()=>{

	localStorage.setItem("cart",JSON.stringify([]));
	location.reload();

});

checkoutBtn.addEventListener('click',()=>{
	let total = cart.reduce((x,item)=>x+=(item.productPrice*item.quantity),0);
	fetch(`http://localhost:4000/api/orders/`,
		{
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${token}`
			},
			body: JSON.stringify({
				totalAmount: total,
				products: cart
			}
			)
		}
	)
	.then(result => result.json())
	.then(result => {
		
		if(result){
			localStorage.setItem("cart",JSON.stringify([]));
			Swal.fire(
			  'Order Placed!',
			  `Thank you for ordering!`,
			  'success'
			).then(()=>window.location.assign('./products.html'))
		} else {
			alert(`Something went wrong.`)
		}
	})
});


deleteCart2.addEventListener('click',()=>{

	localStorage.setItem("cart",JSON.stringify([]));
	location.reload();

});

checkoutBtn2.addEventListener('click',()=>{

	let total = cart.reduce((x,item)=>x+=(item.productPrice*item.quantity),0);
	fetch(`http://localhost:4000/api/orders/`,
		{
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${token}`
			},
			body: JSON.stringify({
				totalAmount: total,
				products: cart
			}
			)
		}
	)
	.then(result => result.json())
	.then(result => {
		
		if(result){
			localStorage.setItem("cart",JSON.stringify([]));
			Swal.fire(
			  'Order Placed!',
			  `Thank you for ordering!`,
			  'success'
			).then(()=>window.location.assign('./products.html'))
		} else {
			alert(`Something went wrong.`)
		}
	})
});


