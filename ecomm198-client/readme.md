# Zuitt Shop

### E-Commerce frontend

Notes:
- Add a new admin user and update it as admin in your database.
- Client was created from index to register > login > createProduct > products page. Therefore, the notes are added in the same sequence.

## Resources

#### JS DOM
- [Introduction to DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)
- [document.getElementById()](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById)
- [document.querySelector()](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector)
- [element.innerHTML](https://developer.mozilla.org/en-US/docs/Web/API/Element/innerHTML)
- [element.addEventListener()](https://www.w3schools.com/jsref/met_element_addeventlistener.asp)
- [JS Dom Events](https://www.w3schools.com/js/js_htmldom_events.asp)

#### Getting Data from the API and localStorage
- [The Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
- [JS localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)
- [localStorage getItem()](https://developer.mozilla.org/en-US/docs/Web/API/Storage/getItem)
- [localStorage setItem()](https://developer.mozilla.org/en-US/docs/Web/API/Storage/setItem)

#### Cloudinary Resources
- [Cloudinary Setup](https://css-tricks.com/image-upload-manipulation-react/)
- [Upload in Cloudinary using Fetch API](https://blog.johnnyreilly.com/2018/03/25/uploading-images-to-cloudinary-with-fetch)

#### JS Dom Exercises
- [w3resource](https://www.w3resource.com/javascript-exercises/javascript-dom-exercises.php)
