const Product = require("../models/Product");

module.exports.addProduct = (req,res) => {

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		image: req.body.image

	})

	newProduct.save()
	.then(product => res.send(product))
	.catch(error => res.send(error))


}

module.exports.getAllProducts = (req,res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

//get single 
module.exports.getSingleProduct = (req,res) => {


	//let id = req.params.id

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

//update: name, description and price
module.exports.updateProduct = (req,res) => {

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		image: req.body.image

	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

module.exports.archive = (req,res) => {

	//updates object will contain the field/fields to update and its new value.
	let updates = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

module.exports.activate = (req,res) => {


	let updates = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.getInactiveProducts = (req,res) => {

	Product.find({isActive:false})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}
