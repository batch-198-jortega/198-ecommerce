const Order = require("../models/Order");

const Product = require("../models/Product");

module.exports.addOrder = async (req,res) => {


	if(req.user.isAdmin) {
		return res.send("Action Forbidden")
	}

	let newOrder = new Order({

		userId: req.user.id,
		totalAmount: req.body.totalAmount,
		products: req.body.products

	})

	let orderId = await newOrder.save()
	.then(order => order._id)
	.catch(err => res.send(err))

	newOrder.products.forEach(product => {

		Product.findById(product.productId).then(foundProduct => {

				let order = {
					orderId: orderId,
					quantity: product.quantity
				}

				
				foundProduct.orders.push(order);

				foundProduct.save()
			})

	})

	res.send({message: "Order Successful."})
}


module.exports.getAllOrders = (req,res) => {

	Order.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

module.exports.getUserOrders = (req,res) => {


	Order.find({userId:req.user.id})
	.then(result => 

{		console.log(result)
		res.send(result)})
	.catch(err => res.send(err))


}